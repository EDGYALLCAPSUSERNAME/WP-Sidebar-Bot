World Power Sidebar Bot
=======================
What is World Power Sidebar Bot?
----------------
This is a reddit bot that updates the sidebar of [/r/worldpowers/](http://www.reddit.com/r//r/worldpowers/).

Setup
-----
In the WPSideBarBot.py file, replace the constants at the top of the file with the
required information. You must have python 2 and praw installed for this to work.

I recommend running a cronjob (OS X, and Linux) or Task Scheduler (Windows) to run
this script everday at 11:59PM. The script will not do anything unless it is
midnight. So, it will try until it hits midnight, update the sidebar and then exit.
